// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// icon-color: cyan; icon-glyph: magic;
let widget = new ListWidget()

let gradient = new LinearGradient()
gradient.colors = [new Color("#e3f2fd",1), new Color("#e3f2fd",0.8)]
gradient.locations = [0, 1]
widget.backgroundGradient = gradient

let weeks = new Array("SUN", "MON", "TUES", "WED", "THUR", "FRI", "SAT")
let day = new Date().getDay()
let date = (new Date().getMonth() + 1) + '·' + new Date().getDate() + ' ' + weeks[day]
let text01 = widget.addText(date)
text01.font = Font.boldSystemFont(16)
text01.textColor = Color.black()

widget.addSpacer(5)

let text02 = widget.addText('👩🏻‍❤️‍💋‍👨🏻  ' + getDistanceSpecifiedTime('2018/11/07') + '')
text02.font = Font.lightSystemFont(14)
text02.textColor = new Color('#999999')

widget.addSpacer(5)

let text03 = widget.addText('🪪 ' + getDistanceSpecifiedTime('2019/07/20') + '')
text03.font = Font.lightSystemFont(14)
text03.textColor = new Color('#999999')

widget.addSpacer(5)

let text04 = widget.addText('👰🏻‍♀️ ' + getDistanceSpecifiedTime('2021/01/02') + '')
text04.font = Font.lightSystemFont(14)
text04.textColor = new Color('#999999')

let text05 = widget.addText('🎆 ' + getDistanceSpecifiedTime('2023/01/22') + '')
text05.font = Font.lightSystemFont(14)
text05.textColor = new Color('#999999')

widget.addSpacer(5)

Script.setWidget(widget)
Script.complete()


function getDistanceSpecifiedTime(dateTime) {
    // 指定日期和时间
    var StartTime = new Date(dateTime);
    // 当前系统时间
    var NowTime = new Date();
    var t = NowTime.getTime() - StartTime.getTime();
    var d = Math.floor(t / 1000 / 60 / 60 / 24);
    var h = Math.floor(t / 1000 / 60 / 60 % 24);
    var m = Math.floor(t / 1000 / 60 % 60);
    var s = Math.floor(t / 1000 % 60);
    return Math.abs(d+1);
}